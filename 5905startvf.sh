#!/bin/bash
deviceID="496559fa"
result="$(adb -s $deviceID shell dumpsys power | grep -c "mWakefulness=Awake")"
if [ "$result" == 1 ]; then
    echo "Screen is already on."
else
    echo "Turning screen on."
    adb -s $deviceID shell input keyevent 26
    sleep 2
fi
echo "Closing previous VNCserver"
adb -s $deviceID shell "am force-stop com.vmlite.vncserver"
echo "Starting VNCserver"
sleep 5
adb -s $deviceID shell "am start -a android.intent.action.MAIN -n com.vmlite.vncserver/.MainActivity"
adb -s $deviceID shell /data/data/com.vmlite.vncserver/files/vmlitevncserver
adb -s $deviceID forward tcp:5803 tcp:5803
adb -s $deviceID forward tcp:5903 tcp:5903
start "C:\empellovnc\vnc\5905.vnc"
exit