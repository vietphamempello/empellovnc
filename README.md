# Empello VNC #

### Introduction ###

VNC (Virtual Network Computing) is an alternative way share screen, similar to Teamviewer. It creates opens up a connection on a device and the other device connects via a specific port number. This new method of connecting to mobile phones should give the analyst more control and better stability. A phone is connected to a laptop, which is also connected to the broadband. As long as the laptop is connected to the broadband we are able to send ADB commands to the phone and control the phone even though its not connected to 4G or the wifi. The broadband speed will determine how smooth analysts will be able to test.

### Requirements ###

* Rooted Android Phone - It can be running any version of Android, it has only been tested on Galaxy S5 and Galaxy S6 running on Android 6.0 and above.
* A Laptop with at least 4GB RAM, SSD and USB ports. We do however send the laptop with an additional 4 USB port dongle by Anker.
* Correct data cables - Some cables are not ADB friendly.
* ADB installed onto the laptop - https://developer.android.com/studio/command-line/adb
* TightVNC viewer installed onto the laptop - https://www.tightvnc.com/download.php
* DFMirage Driver installed onto the laptop - https://www.tightvnc.com/download.php (scroll down the page)
* Android USB drivers installed onto the laptop - https://developer.samsung.com/galaxy/others/android-usb-driver-for-windows
* Git bash terminal installed onto the laptop - https://gitforwindows.org/
* Python 2.7 installed onto the laptop - https://www.python.org/download/releases/2.7/
* Notepad++ - https://notepad-plus-plus.org/download/v7.5.7.html
* Teamviewer Host installed onto the laptop - https://www.teamviewer.com/en/download/windows/
* VMLite VNC Server installed onto the phone - https://play.google.com/store/apps/details?id=com.vmlite.vncserver
* empellovnc script from Bitbucket - https://bitbucket.org/vietphamempello/empellovnc/src/master/

### Setting up the phone ###

Root the phone via https://autoroot.chainfire.eu/

Put the phone into developer mode by going into the settings, about phone, and tapping on Build Number 7 times. The developer options will be on in the main options menu. Go into developer options and enable USB Debugging, if the phone is plugged into the laptop it a notification will pop up on the phone asking you if you trust the computer, press accept.

A few issues you might run into during this process is that the laptop might not recognise the phone. This could be caused by an incorrect cable or faulty USB port. The cable should be both a charging and data cable, you would of needed this to root the phone.

Install the VMlite VNC server onto the phone, we have it purchased on the empelloroot@gmail.com. Open up the app and click go into the settings (cogwheel).

* Change the VNC Port from 5900 to  5901 or any number above. Just make sure the port is available and not used up by another device.
* Change the Screen Scaling from 1 to Automatic. This helps prevent the landscape crash bug, but it also makes the viewer a lot bigger.

Press Start and grant the app super user permission, and then it should say that the server is running with an IP and a port.  The IP is not as important as the port number, as there are 2 different port numbers. One for the VNC viewer app to connect and the other for a browser to connect. 

* Start - This button excutes the bash script
  * The bash script checks if the screen is on, if not it will turn it self on.
  * It will close the previous VNC server
  * Start a new VNC server and forward the correct tcp ports.
  * Execute the vnc user setting.
* Reboot - reboots the phone by sending an adb command
* Overview - brings up the most recent app, the current button on the TightVNC does not work.
* Power - this sends a power button command to the phone (turns on screen or off) useful for when the phone has been inactive for long periods of time.
* Unstuck - If the phone reboots and pressing start doesn't work then its most likely stuck. clicking this button just sends a click command.
* Teamviewer - this button will start up the activity for Teamviewer Host.

###  Current issues  ###

* Galaxy S6 are unable to scroll up using the mouse.
* None rooted phones are unable to use the Unstuck
