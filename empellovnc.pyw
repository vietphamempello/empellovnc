import Tkinter
import subprocess
import os
from Tkinter import *
import ttk
import subprocess as sub

#Main
root = Tk()
root.title('Empello - VNC')
root.resizable(width=FALSE, height=FALSE)
#Create the window GUI
root.geometry('{}x{}'.format(400, 280))

# Variable for the devices. Change the network and the device ID
o2phone = '15cd97ca'
vfphone = '496559fa'
o2phone2 = '0815f872ca494004'

# Buttons for the 1st Column
button_1a = Tkinter.Button(root, text ="Start O2",command = lambda: subprocess.call('C:/Users/User/Desktop/empellovnc/empellovnc/5906starto2.sh', shell=True, ))
button_1a.grid(row=0, column=0, pady=3)
button_1b = Tkinter.Button(root, text ="Reboot O2", command = lambda: subprocess.call("adb -s " + o2phone + " shell reboot", shell=True))
button_1b.grid(row=1, column=0, pady=3)
button_1c = Tkinter.Button(root, text ="Overview O2", command = lambda: subprocess.call("adb -s " + o2phone + " shell input keyevent KEYCODE_APP_SWITCH", shell=True))
button_1c.grid(row=2, column=0, pady=3)
button_1d = Tkinter.Button(root, text ="Power O2", command = lambda: subprocess.call("adb -s " + o2phone + " shell input keyevent 26", shell=True))
button_1d.grid(row=3, column=0, pady=3)
button_1e = Tkinter.Button(root, text ="Unstuck O2", command = lambda: subprocess.call("adb -s " + o2phone + " shell input touchscreen tap 523 462", shell=True))
button_1e.grid(row=4, column=0, pady=3)
button_1f = Tkinter.Button(root, text ="Teamviewer", command = lambda: subprocess.call("adb -s " + o2phone + " shell monkey -p com.teamviewer.host.market -v 1", shell=True))
button_1f.grid(row=5, column=0, pady=3)

# Buttons for the 2nd Column
button_2a = Tkinter.Button(root, text ="Start VF", command = lambda: subprocess.call('C:/Users/User/Desktop/empellovnc/empellovnc/5903startvf.sh', shell=True))
button_2a.grid(row=0, column=1, pady=3)
button_2b = Tkinter.Button(root, text ="Reboot VF", command = lambda: subprocess.call("adb -s " + vfphone + " shell reboot", shell=True))
button_2b.grid(row=1, column=1, pady=3)
button_2c = Tkinter.Button(root, text ="Overview VF", command = lambda: subprocess.call("adb -s " + vfphone + " shell input keyevent KEYCODE_APP_SWITCH", shell=True))
button_2c.grid(row=2, column=1, pady=3)
button_2d = Tkinter.Button(root, text ="Power VF", command = lambda: subprocess.call("adb -s " + vfphone + " shell input keyevent 26", shell=True))
button_2d.grid(row=3, column=1, pady=3)
button_2e = Tkinter.Button(root, text ="Unstuck VF", command = lambda: subprocess.call("adb -s " + vfphone + " shell input touchscreen tap 523 462", shell=True))
button_2e.grid(row=4, column=1, pady=3)
button_2f = Tkinter.Button(root, text ="Teamviewer", command = lambda: subprocess.call("adb -s " + vfphone + " shell monkey -p com.teamviewer.host.market -v 1", shell=True))
button_2f.grid(row=5, column=1, pady=3)

# Buttons for the 3rd Column
button_3a = Tkinter.Button(root, text ="Start O2", command = lambda: subprocess.call('C:/Users/User/Desktop/empellovnc/empellovnc/5907starto2.sh', shell=True))
button_3a.grid(row=0, column=2, pady=3)
button_3b = Tkinter.Button(root, text ="Reboot O2", command = lambda: subprocess.call("adb -s " + o2phone2 + " shell reboot", shell=True))
button_3b.grid(row=1, column=2, pady=3)
button_3c = Tkinter.Button(root, text ="Overview O2", command = lambda: subprocess.call("adb -s " + o2phone2 + " shell input keyevent KEYCODE_APP_SWITCH", shell=True))
button_3c.grid(row=2, column=2, pady=3)
button_3d = Tkinter.Button(root, text ="Power O2", command = lambda: subprocess.call("adb -s " + o2phone2 + " shell input keyevent 26", shell=True))
button_3d.grid(row=3, column=2, pady=3)
button_3e = Tkinter.Button(root, text ="Unstuck O2", command = lambda: subprocess.call("adb -s " + o2phone2 + " shell input touchscreen tap 523 462", shell=True))
button_3e.grid(row=4, column=2, pady=3)
button_3f = Tkinter.Button(root, text ="Teamviewer", command = lambda: subprocess.call("adb -s " + o2phone2 + " shell monkey -p com.teamviewer.host.market -v 1", shell=True))
button_3f.grid(row=5, column=2, pady=3)

top_frame = Frame(root, width = 450, height=50, pady=3)

#Configure rows
root.grid_rowconfigure(10, weight=1)
#Configure columns
root.grid_columnconfigure(2, weight=1)
root.mainloop()