#!/bin/bash
deviceID="15cd97ca"
result="$(adb -s $deviceID shell dumpsys power | grep -c "mWakefulness=Awake")"
if [ "$result" == 1 ]; then
    echo "Screen is already on."
else
    echo "Turning screen on."
    adb -s $deviceID shell input keyevent 26
    sleep 2
fi
echo "Closing previous VNCserver"
adb -s $deviceID shell "am force-stop com.vmlite.vncserver"
echo "Starting VNCserver"
sleep 5
adb -s $deviceID shell "am start -a android.intent.action.MAIN -n com.vmlite.vncserver/.MainActivity"
adb -s $deviceID shell /data/data/com.vmlite.vncserver/files/vmlitevncserver
adb -s $deviceID forward tcp:5806 tcp:5806
adb -s $deviceID forward tcp:5906 tcp:5906
start "C:\Users\User\Desktop\empellovnc\vnc\5906.vnc"
exit