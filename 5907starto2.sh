#!/bin/bash
deviceID="0815f872ca494004"

result="$(adb -s $deviceID shell dumpsys power | grep -c "mWakefulness=Awake")"
if [ "$result" == 1 ]; then
    echo "Screen is already on."
else
    echo "Turning screen on."
    adb -s $deviceID shell input keyevent 26
    sleep 2
fi
echo "Closing previous VNCserver"
adb -s $deviceID shell "am force-stop com.vmlite.vncserver"
echo "Starting VNCserver"
sleep 5
adb -s $deviceID shell "am start -a android.intent.action.MAIN -n com.vmlite.vncserver/.MainActivity"
#adb -s $deviceID shell "/data/data/com.vmlite.vncserver/files/vmlitevncserver"
#adb -s $deviceID shell "adb shell"
adb -s $deviceID shell "cd data/local"
adb -s $deviceID shell "mkdir -p tmp"
adb -s $deviceID shell "cd /data/local/tmp"
adb -s $deviceID shell "root cp /data/data/com.vmlite.vncserver/files/vmlitevncserver ."
adb -s $deviceID shell "chmod 777 ./vmlitevncserver"
adb -s $deviceID shell "root ./vmlitevncserver"
adb -s $deviceID forward tcp:5807 tcp:5807
adb -s $deviceID forward tcp:5907 tcp:5907
start "C:/Users/User/Desktop/empellovnc/empellovnc/5908.vnc"
exit